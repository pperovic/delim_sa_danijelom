/**
 * Created by Predrag on 26/04/2016.
 */
function checkWinAndAnimateOnClientSide()
{
    // for dev test
    /*
    for (var reel = 0; reel < config.gameSize.reelCount; reel++)
    {
        console.log("reel:" + reel);

        for (var iterator = 3; iterator > 0; iterator--){
            console.log(reelStripeElements[reel].getChildAt(iterator)._texture.baseTexture.uid);
            //console.log(reelStripeElements[reel].getChildAt(iterator)._texture.baseTexture.imageUrl);
        }
    } */
    // for real
    for (var t=3; t>0; t--) {
        var winFramePositions= [];
        var hasByNow= 1;
        winFramePositions += getWinFrameImage(reelStripeElements[0].getChildAt(t).getGlobalPosition().x.toString(),
            reelStripeElements[0].getChildAt(t).getGlobalPosition().y.toString());
        for (var reel = 1; reel < config.gameSize.reelCount; reel++) {
            console.log("reel:" + reel);

            var go_next= false;
            for (var iterator = 3; iterator > 0; iterator--) {

                if (reelStripeElements[0].getChildAt(t)._texture.baseTexture.uid ==
                    reelStripeElements[reel].getChildAt(iterator)._texture.baseTexture.uid){

                    console.log("Element pos: " +  reelStripeElements[reel].getChildAt(iterator).getGlobalPosition().x.toString()+ " and " +
                        reelStripeElements[reel].getChildAt(iterator).getGlobalPosition().y.toString() );

                    winFramePositions +=getWinFrameImage(reelStripeElements[reel].getChildAt(iterator).getGlobalPosition().x.toString(),
                                            reelStripeElements[reel].getChildAt(iterator).getGlobalPosition().y.toString());
                    hasByNow++;     console.log(" has by now " +hasByNow);
                    go_next = true;

                    break;
                }
            }
            //  not needed to go any further
            if(go_next == false)
            {
               // winFramePositions.removeChildren();
                break;
            }
        }
        if(hasByNow > 2) { //animate the win
            console.log("--------- wins: " + winFramePositions);
            reelStripeElements[0].addChild(winFramePositions[0]);
            renderer.render(checkWinAndAnimateOnClientSide());
            requestAnimationFrame(function(){checkWinAndAnimateOnClientSide()});
        }
    }
}

function setSpinDisable(){
    console.log("usao");
    //document.body.getElementById("spinimg") = "resources/img/spin3no.jpg";
}

function getWinFrameImage(tmpx, tmpy){
    var texture_tmp = PIXI.Texture.fromImage("resources/img/winframe.png",PIXI.SCALE_MODES.LINEAR);
    var reelImg = new PIXI.Sprite(texture_tmp);
    reelImg.anchor.x = 0;
    reelImg.anchor.y = 0;
    reelImg.width =  reelImageWidth;
    reelImg.height=  reelImageHeight;
    reelImg.position.x = tmpx;
    reelImg.position.y = tmpy;
    return reelImg;
}