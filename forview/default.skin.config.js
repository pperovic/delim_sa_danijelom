{  variables:[],
 loader:[
	{id:"default-gamelayout", type:"config", url:"gamelayout/default-gamelayout.config.js"},
	{id:"default-reel-foreground", type:"config", url:"reelforeground/default-reel-foreground.config.js"},
	{id:"default-reels", type:"config", url:"reels/default-reels.config.js"},
	{id:"default-bet-panel", type:"config", url:"bet-panel/default-bet-panel.config.js"},
	{id:"default-info-bar", type:"config", url:"infobar/default-info-bar.config.js"},
	{id:"default-logo", type:"config", url:"logo/default-logo.config.js"},
	{id:"default-winlines", type:"config", url:"winlines/default-winlines.config.js"},
	
   /*
   {id:"default-bet-panel", type:"config", url:"bet-panel/default-bet-panel.config.js"},
   {id:"default-gamelayout", type:"config", url:"gamelayout/default-gamelayout.config.js"},
   
   {id:"default-free-spin-panels", type:"config", url:"freespinpanels/default-free-spin-panels.config.js"},

   {id:"default-reel-gaps", type:"config", url:"reel-gaps/default-reel-gaps.config.js"},
   {id:"default-expanding-wilds", type:"config", url:"expandingwilds/default-expanding-wilds.config.js"},
   
   {id:"default-paytable", type:"config", url:"paytable/default-paytable.config.js"},

   {id:"default-bigwin", type:"config", url:"bigwin/default-big-win.config.js"}
   */
 ]
}
